var videos = [];

var cardNext = null;
var haveNext = true;
var controlTime = 0;

videos = (typeof getStorage('videos', true) !== 'undefined' ? getStorage('videos', true) : []);

setTimeout(function() {
    showPage('player');
}, 0);



function validateStorage(){
    if (typeof(Storage) !== "undefined") {
        return true;
    }
    return false;
}

function getStorage(index, json) {
    if (validateStorage() == false) {
        return 'ERROR: Storage not supported!';
    }
    return (json && typeof localStorage[index] !== 'undefined' ? JSON.parse(localStorage[index]) : localStorage[index]);
}

function setStorage(index, value, json) {
    if (validateStorage() == false) {
        return 'ERROR: Storage not supported!';
    }
    if (json) {
        value = JSON.stringify(value);
    }
    localStorage[index] = value;
}

function showPage(page) {
    switch (page) {
        case 'player':
            document.getElementById('content').innerHTML = buildPlayer();
            cardNext = document.getElementById('divCardNext');
            if (videos.length > 0) {
                setVideo(0, 0);
            }
            break;
        default:
    }
}

function buildPlayer() {
    var html =
        '<div class="row">'+
        '    <div class="col '+( videos.length > 1 ? "size80" : "size100" )+' sizeM100 clear-y clear-m-y">'+
        '        <div class="row">'+
        '            <div class="col size100 clear clear-m">'+
        '                <h2 id="currentVideoTitle" class="">Title video - EP 001</h2>'+
        '            </div>'+
        '        </div>'+
        '    </div>'+
        (
            videos.length > 1 ?
                '    <div class="col size20 clear-y clear-m-y not-m">'+
                '        <div class="row">'+
                '            <div class="col size100 clear clear-m">'+
                '                <h3 id="txtNextD">Próximo</h3>'+
                '            </div>'+
                '        </div>'+
                '    </div>'+
                '</div>'
            :
                ''
        )+
        '<div class="row">'+
        '    <div class="col '+( videos.length > 1 ? "size80" : "size100" )+' sizeM100">'+
        '        <div class="row">'+
        '            <div class="col size100 clear clear-m">'+
        '                <div class="div-player">'+
        '                    <div class="div-card-next pabsolute ph10 pv10 cursor" id="divCardNext" style="display: none;">'+
        '                        <p class="truncate" id="pCardNext"></p>'+
        '                    </div>'+
        '                    <video controls id="videoPlayer" class="player" muted >'+
        '                        <source id="currentVideo" src="" />'+
        '                    </video>'+
        '                </div>'+
        '            </div>'+
        '        </div>'+
        '        <div class="row">'+
        '            <div class="col size100 clear-x clear-m-x">'+
        '                <input id="ipFile" type="file" multiple="true" name="" value="" onchange="getFiles(files)" />'+
        '            </div>'+
        '        </div>'+
        '    </div>'+
        (
            videos.length > 1 ?
                '    <div class="row not-d">'+
                '        <div class="col sizeM100">'+
                '            <div class="row">'+
                '                <div class="col size100 clear clear-m">'+
                '                    <h3 id="txtNextM">Próximo</h3>'+
                '                </div>'+
                '            </div>'+
                '        </div>'+
                '    </div>'+
                '    <div class="col size20 sizeM100" id="divNext"></div>'
            :
                ''
        )+
        '</div>'
    ;
    return html;
}


function getFiles(file) {
    localStorage.removeItem('videos');
    for (var i = 0; i < file.length; i++) {
        videos.push({
            status: (i > 0 ? false : true),
            currentTime: 0,
            totalTime: 0,
            video: {
                name: file[i].name,
                url: file[i].path
            }
        });
    }
    showPage('player');
}

function setVideo(p, a){
    var html = '';
    videos[p].status = false;
    videos[a].status = true;
    haveNext = true;
    if (a + 1 >= videos.length) {
        if (videos.length > 1) {
            document.getElementById('txtNextD').innerHTML = 'Recomeçar';
            document.getElementById('txtNextM').innerHTML = 'Recomeçar';
        }
        haveNext = false;
    }else{
        if (videos.length > 1) {
            document.getElementById('txtNextD').innerHTML = 'Próximo';
            document.getElementById('txtNextM').innerHTML = 'Próximo';
        }
    }
    for (var i = a; i < videos.length; i++) {
        html += listVideo(i, a);
    }
    for (var i = 0; i < a; i++) {
        html += listVideo(i, a);
    }
    if (videos.length > 1) {
        document.getElementById('divNext').innerHTML = html;
    }
    window.scrollTo(0, 0);
    setStorage('videos', videos, true);
}

function showCardNext(i) {
    if ( ( ( videos[i].currentTime * 100 ) / videos[i].totalTime ) > 90 ) {
        if (haveNext) {
            if (cardNext.style.display == 'none') {
                cardNext.style.display = '';
            }
            cardNext.onclick = function() {
                setVideo(i, (i+1));
            };
            document.getElementById('pCardNext').innerHTML = '<small>Próximo episódio</small><br/>' + videos[(i+1)].video.name;
        }
    }else{
        if (cardNext.style.display == '') {
            cardNext.style.display = 'none';
        }
    }
}

function setCurrentTime(i) {
    videos[i].currentTime = document.getElementById('videoPlayer').currentTime;
    if (controlTime == 0 || videos[i].currentTime < controlTime) {
        controlTime = videos[i].currentTime;
    }else{
        if (videos[i].currentTime - controlTime >= 5) {
            setStorage('videos', videos, true);
            controlTime = 0;
        }
    }
    showCardNext(i);
}

function setTotalTime(i, duration) {
    videos[i].totalTime = duration;
    if (videos[i].status) {
        return false;
    }
    document.getElementById('div-time-'+i).innerHTML = '<div class="div-time" style="width: '+ ( ( videos[i].currentTime * 100 ) / videos[i].totalTime ) +'%"></div>';
}

function listVideo(i, a) {
    if (cardNext.style.display == '') {
        cardNext.style.display = 'none';
    }
    var html = '';
    if (videos[i].status) {
        document.getElementById('currentVideo').setAttribute('src', videos[i].video.url);
        document.getElementById('currentVideoTitle').innerHTML = (videos[i].video.name).replace("_", " ");
        // document.getElementById('videoPlayer').load();
        document.getElementById('videoPlayer').play();
        // document.getElementById('ipFile').style.display = 'none';
        document.getElementById('videoPlayer').currentTime = videos[i].currentTime;
        document.getElementById('videoPlayer').ontimeupdate = function() {setCurrentTime(i);};
        document.getElementById('videoPlayer').onloadedmetadata = function() {setTotalTime(i, this.duration);};
    }else{
        html +=
            '<div class="row">'+
            '    <div title="'+videos[i].video.name+'" class="col size100 mb10 '+( ( i + 1 < videos.length ) && ( ( a + 1 ) == i ) ? "pb10 border-bottom mb5" : "" )+' clear cursor" onclick="setVideo('+a+', '+i+')">'+
            '        <div class="player-min">'+
            '             <video style="width: 100%; height: 100%;" onloadedmetadata="setTotalTime('+i+', this.duration)" onpause="setStorage(\'videos\', videos, true);" onplay="setStorage(\'videos\', videos, true);" onplaying="setStorage(\'videos\', videos, true);" onwaiting="setStorage(\'videos\', videos, true);">'+
            '                 <source src="'+videos[i].video.url+'">'+
            '             </video>'+
            '            <div class="player-min-title">'+
            '                <p class="truncate ph5">'+(videos[i].video.name).replace("_", " ")+'</p>'+
            '            </div>'+
            '        </div>'+
            '            <div class="prelative div-time-background" id="div-time-'+i+'"></div>'+
            '    </div>'+
            '</div>'
        ;
    }
    return html;
}
